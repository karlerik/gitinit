#!/usr/bin/env ruby
require "net/http"
require "json"
require 'open3'

#host = `dmidecode -s system-product-name`.strip
#uuid = `dmidecode -s system-version`.strip

host = "https://i-dev.itcollege.ee"
uuid = "15fabe32-3b7e-465a-a24a-489a58a79180"
root = "/root"
scripts_folder = "scripts"

# Fetch lab name and GIT address
begin
  uri = URI("#{host}/labinfo.json?uuid=#{uuid}")
  res = Net::HTTP.get_response(uri)
  json = JSON.parse(res.body)
  begin
    name = json["lab"]["name"]
  rescue NoMethodError
    abort("Could not retrieve lab name")
  end
  begin
    git = json["lab"]["config"]["git"]
  rescue NoMethodError
    abort("Could not retrieve lab GIT address")
  end
  rescue SocketError
  puts "Host is not reachable"
end

# Clone the repo
cmd = "cd #{root} && git clone --progress https://karlerik@bitbucket.org/karlerik/labchecks.git #{scripts_folder}"
git_success = false

Open3.popen3(cmd) do |stdin, stdout, stderr, wait_thr|
  for line in stderr do
    #puts "- #{line}"
    if line.include? "remote: Total"
      git_success = true
    end 
    if line.include? "already exists"
      git_success = false
      abort("Lab is already loaded with content. You can refetch by restarting lab or 'systemctl reload labchecks'")
    end
  end
end

# Check if lab's branch is created
if git_success
  puts "Repo cloned. Seeking for branch named #{name}..."
  branch_found = false
  Open3.popen3("cd #{root}/#{scripts_folder} && git branch -a") do |stdin, stdout, stderr, wait_thr|
    for line in stdout do
      #puts "+ #{line}"
      if line.include? name
        branch_found = true
      end 
    end
  end
end

if branch_found
  # Branch found. Checking out
  puts "Found our branch. Checking out..."
  branch_switched = false
  Open3.popen3("cd #{root}/#{scripts_folder} && git checkout #{name}") do |stdin, stdout, stderr, wait_thr|
    for line in stderr do
      #puts "+ #{line}"
      if line.include? "Switched to a new branch '#{name}'"
        branch_switched = true
      end 
    end
  end

  if branch_switched
    puts "Setting branch '#{name}' as upstream"
    upstream_switched = false
    Open3.popen3("cd #{root}/#{scripts_folder} && git branch --set-upstream-to origin/#{name}") do |stdin, stdout, stderr, wait_thr|
      for line in stdout do
        #puts "+ #{line}"
        if line.include? "track remote branch"
          upstream_switched = true
        end 
      end
    end
  end

  if upstream_switched
    puts "'#{name}' is now set up. We're done. Fuck."
  else
    puts "Could not set '#{name}' as upstream. Idk why. Let's start over I guess."
  end
else
  # Creating new branch 
  puts "No #{name} branch found. Creating new branch named '#{name}'"
  branch_init = false
  Open3.popen3("cd #{root}/#{scripts_folder} && git checkout -b #{name} master") do |stdin, stdout, stderr, wait_thr|
    for line in stderr do
      #puts "+ #{line}"
      if line.include? "new branch"
        branch_init = true
      end
    end
  end

  if branch_init
    # Pushing new branch to repo
    puts "New branch inited. Pushing to the clouds"
    branch_completed = false
    Open3.popen3("cd #{root} && git push origin #{name}") do |stdin, stdout, stderr, wait_thr|
      for line in stderr do
        #puts "+ #{line}"
        if line.include? "-> #{name}"
          branch_completed = true
        end
      end
    end
  end

  if branch_completed
    puts "Branch is pushed."
  else
    puts "Branch was not pushed."
  end
end