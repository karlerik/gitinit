#!/bin/bash
echo "Starting to deploy git init mechanism to lab"

if ! [ $(id -u) = 0 ]; then
   echo "Please run this script as root."
   exit 1
fi

SYSTEMD="/etc/systemd/system"

echo "Installing Ruby and Git..."
apt-get install -y -qq git ruby

if [[ $? > 0 ]]; then
  echo "Could not install Ruby and Git."
  exit
else
  echo "Cloning gitinit repo to /opt/gitinit..."
  git clone https://karlerik@bitbucket.org/karlerik/gitinit.git /opt/gitinit
  if [[ $? > 0 ]]; then
    echo "Could not clone gitinit repo. Exiting"
    exit
  else
    echo "Creating labinit.service..."
    cat > $SYSTEMD/gitinit.service <<EOF
[Unit]
Description=Deploy Lab's git content to lab
After=network.target

[Service]
Type=simple
User=root
WorkingDirectory=/root
ExecStart=/usr/bin/ruby /opt/gitinit/deploy.rb
Restart=no

[Install]
WantedBy=multi-user.target
EOF

echo "Enabling gitinit service on startup"
systemctl daemon-reload &&
systemctl enable gitinit.service &&
systemctl start gitinit.service &&
echo "Git init mechanism installed!"
  fi
fi