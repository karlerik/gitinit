# GIT Init Service

Simple application to auto-deploy configuration scripts to I-Tee lab(s).

## Features

* Deploy scripts to lab from generic lab branch
* Auto-initialize new generic lab branch from skeleton
* Fetch scripts from external repos

### Prerequisites

* Debian/Ubuntu
* Systemd

### Installing

Run this one-liner in router lab to install gitinit


```wget -O - https://bitbucket.org/karlerik/gitinit/raw/HEAD/install.sh | bash```

Clones the repo to /opt/gitinit and sets up the service file to autofetch lab(s) git repo on startup.

### Deployment

Verify that gitinit was successfully deployed by taking a peek at logs with ```systemctl status gitinit```

## Authors

* **Karl Erik Õunapuu** - *Initial work* - [karlerikounapuu](https://github.com/karlerikounapuu)

## License

This project is licensed under the MIT License